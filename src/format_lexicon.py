#!/usr/bin/env python
# coding: utf-8

import re
import pandas as pd
import unicodedata


def split(f, sound_reg):
    f2 = list(re.split(sound_reg, f))
    return " ".join([x for x in f2 if x])

def parse_verb(lines):
    """ Parse a single verb in the raw (custom) format.
    Args:
        lines (list): one string per line, the set of lines represent the paradigm of a single verb.

    Returns:
        lexeme (str): a lexeme identifier
        paradigm (dict): a dictionnary of cells to forms, with an added "class" key giving the inflection class.
    """
    cell_map = {"PresIndic": ["prs.ind.1sg",
                              "prs.ind.2sg",
                              "prs.ind.3sg",
                              "prs.ind.1pl",
                              "prs.ind.2pl",
                              "prs.ind.3pl"],
                "PretImpIndic": ["pst.impf.ind.1sg",
                                 "pst.impf.ind.2sg",
                                 "pst.impf.ind.3sg",
                                 "pst.impf.ind.1pl",
                                 "pst.impf.ind.2pl",
                                 "pst.impf.ind.3pl"],
                "PretPerfIndic": [
                    "pst.pfv.ind.1sg",
                    "pst.pfv.ind.2sg",
                    "pst.pfv.ind.3sg",
                    "pst.pfv.ind.1pl",
                    "pst.pfv.ind.2pl",
                    "pst.pfv.ind.3pl", ],
                "PretMqpfIndic": ["pst.perf.ind.1sg",
                                  "pst.perf.ind.2sg",
                                  "pst.perf.ind.3sg",
                                  "pst.perf.ind.1pl",
                                  "pst.perf.ind.2pl",
                                  "pst.perf.ind.3pl", ],
                "FutImpIndic": ["fut.ind.1sg",
                                "fut.ind.2sg",
                                "fut.ind.3sg",
                                "fut.ind.1pl",
                                "fut.ind.2pl",
                                "fut.ind.3pl", ],
                "Condicional": ["cond.1sg",
                                "cond.2sg",
                                "cond.3sg",
                                "cond.1pl",
                                "cond.2pl",
                                "cond.3pl", ],
                "PresConj": ["prs.sbjv.1sg",
                             "prs.sbjv.2sg",
                             "prs.sbjv.3sg",
                             "prs.sbjv.1pl",
                             "prs.sbjv.2pl",
                             "prs.sbjv.3pl", ],
                "PretImpConj": ["pst.sbjv.1sg",
                                "pst.sbjv.2sg",
                                "pst.sbjv.3sg",
                                "pst.sbjv.1pl",
                                "pst.sbjv.2pl",
                                "pst.sbjv.3pl", ],
                "FutConj": ["fut.sbjv.1sg",
                            "fut.sbjv.2sg",
                            "fut.sbjv.3sg",
                            "fut.sbjv.1pl",
                            "fut.sbjv.2pl",
                            "fut.sbjv.3pl", ],
                "Imperativo": ["imp.2sg",
                               "imp.2pl", ],
                "InfPessoal": ["per.inf.1sg",
                               "per.inf.2sg",
                               "per.inf.3sg",
                               "per.inf.1pl",
                               "per.inf.2pl",
                               "per.inf.3pl", ],
                "Gerundio": ["ger", ],
                "PartPass": ["pst.ptcp.m.sg"]
                }
    header_re = "^.*\{Verbo: ([^,\s]+), Paradigma: ([^,\s]+),\s*$"
    lexeme, classe = re.match(header_re, lines[0]).groups()
    line_re = "^\s*([^\s,\[:]+):\[\s*(.+)\s*\]$"
    paradigm = {"class": classe}
    for l in lines[1:-1]:
        tm, forms = re.match(line_re, l).groups()
        forms = [f.strip() for f in forms.split(", ")]
        cells = cell_map[tm]
        paradigm.update(dict(zip(cells, forms)))
    return lexeme, paradigm


def read_raw(path):
    """ Read and parse the raw file.

    Args:
        path (str): path to the raw file

    Returns:
        a pd.DataFrame with lexemes in rows and cells in columns.
    """
    with open(path, "r", encoding="utf-8") as f:
        chunk = []
        rows = {}
        for line in f:
            chunk.append(line)
            if line[0] == "}":
                lexeme, paradigm = parse_verb(chunk)
                rows[lexeme] = paradigm
                chunk = []
    return pd.DataFrame.from_dict(rows, orient="index")


def add_stress(form):
    """Adds stress before the first syllable of a word.

    Assumes the form is monosyllabic
    """
    if "ˈ" in form:
        return form
    form = re.sub(r"([^ɐaiuəeẽoɔũĩõɛ]*)([ɐaiuəeẽoɔũĩõɛ].*)", r"\1ˈ\2", form)
    return form


def annot_changes(form):
    """ Removes nasalization on the final glide of a diphthong.

    Args:
        form (str): inflected form

    Returns:
        form (str): inflected form, with annotated changes

    """
    repls = [("ɐ̃w̃", "ɐ̃w"),
             ("ɐ̃j̃", "ɐ̃j"),
             ("õj̃", "õj")]
    for chars, repl in repls:
        form = form.replace(chars, repl)
    return form


def main():
    """ Generate the paradigms file from the raw file """

    # Hard coded path, as this is very specific and not meant to be reused on other lexicons
    path = "raw/PronunciarListaVebos2021_v2.txt"
    df = read_raw(path)

    # Infinitive is missing in the paradigms, copy the 1st person of the personal inf.
    df["inf"] = df["per.inf.1sg"]

    # Indexes are lexeme identifiers
    df.index.name = "lexeme"


    cells = ["inf",
             "prs.ind.1sg", "prs.ind.2sg", "prs.ind.3sg",
             "prs.ind.1pl", "prs.ind.2pl", "prs.ind.3pl",
             "pst.impf.ind.1sg", "pst.impf.ind.2sg", "pst.impf.ind.3sg",
             "pst.impf.ind.1pl", "pst.impf.ind.2pl", "pst.impf.ind.3pl",
             "pst.pfv.ind.1sg", "pst.pfv.ind.2sg", "pst.pfv.ind.3sg",
             "pst.pfv.ind.1pl", "pst.pfv.ind.2pl", "pst.pfv.ind.3pl",
             "pst.perf.ind.1sg", "pst.perf.ind.2sg", "pst.perf.ind.3sg",
             "pst.perf.ind.1pl", "pst.perf.ind.2pl", "pst.perf.ind.3pl",
             "fut.ind.1sg", "fut.ind.2sg", "fut.ind.3sg",
             "fut.ind.1pl", "fut.ind.2pl", "fut.ind.3pl",
             "cond.1sg", "cond.2sg", "cond.3sg",
             "cond.1pl", "cond.2pl", "cond.3pl",
             "prs.sbjv.1sg", "prs.sbjv.2sg", "prs.sbjv.3sg",
             "prs.sbjv.1pl", "prs.sbjv.2pl", "prs.sbjv.3pl",
             "pst.sbjv.1sg", "pst.sbjv.2sg", "pst.sbjv.3sg",
             "pst.sbjv.1pl", "pst.sbjv.2pl", "pst.sbjv.3pl",
             "fut.sbjv.1sg", "fut.sbjv.2sg", "fut.sbjv.3sg",
             "fut.sbjv.1pl", "fut.sbjv.2pl", "fut.sbjv.3pl",
             "imp.2sg",
             "imp.2pl",
             "per.inf.1sg", "per.inf.2sg", "per.inf.3sg",
             "per.inf.1pl", "per.inf.2pl", "per.inf.3pl",
             "ger", "pst.ptcp.m.sg"]

    # Normalize notation
    df = df.applymap(lambda x: unicodedata.normalize("NFKC", x))

    # Monosyllabic words are missing stress, adding it explicitly
    df.loc[:, cells] = df[cells].applymap(add_stress)

    # Some homophones lead to duplicate rows, removing them
    df = df.drop_duplicates()

    lexemes = pd.DataFrame(df.loc[:, ["class"]])

    # Changing some annotation conventions
    df = df.applymap(annot_changes)

    # reaver is defective
    df = df.drop("reaver")

    # manual transcription changes
    df.loc["querer", "pst.pfv.ind.1sg"] = "kˈiʃ"
    df.loc["querer", "pst.pfv.ind.3sg"] = "kˈiʃ"
    df.loc["interagir", :] = df.loc["interagir", :].str.replace("ĩtəɾ", "ĩtɛɾ")
    aiar = ['arraiar', 'caiar', 'desmaiar', 'vaiar']
    df.loc[aiar, :] = df.loc[aiar, :].apply(
        lambda forms: forms.str.replace('ɐj', 'aj', n=1))

    # Shaping from wide to long form
    df = df[cells].stack().reset_index()
    df.columns = ["lexeme", "cell", "phon_form"]
    df.insert(loc=0, column='form_id', value=["form_"+str(i+1) for i in range(df.shape[0])])

    # Splitting phonological forms
    sounds = pd.read_csv("./portuguese_phonemes.csv")
    sounds = sorted(sounds.sound_id.tolist(), key=len, reverse=True)
    sound_reg = "(" + "|".join(sounds) + ")"

    df.loc[:, "phon_form"] = df.loc[:, "phon_form"].apply(split,
                                                          sound_reg=sound_reg)

    # Exporting
    df.to_csv("portuguese_paradigms.csv", index=False)

    lexemes["POS"] = "verb"
    lexemes.index.name = "lexeme_id"
    lexemes.reset_index(inplace=True)
    lexemes = lexemes.rename({"class":"inflection_class"}, axis=1)
    lexemes.to_csv("portuguese_lexemes.csv", index=False)


if __name__ == "__main__":
    main()
