from paralex import paralex_factory
from frictionless import Package

package = paralex_factory("European Portuguese Verbal Paradigms in Phonemic Notation",
                          {"cells": {"path": "portuguese_cells.csv"},
                           "forms": {"path": "portuguese_paradigms.csv"},
                           "lexemes": {"path": "portuguese_lexemes.csv"},
                           "sounds": {"path": "portuguese_phonemes.csv"},
                           "features-values": {"path": "portuguese_features.csv"}
                           },
                          licenses=[{'name': 'CC-BY-SA-4.0',
                                     'title': 'Creative Commons Attribution Share-Alike 4.0',
                                     'path': 'https://creativecommons.org/licenses/by-sa/4.0/'}],
                          contributors=[{'title': 'Fernando Perdigão', 'organization': 'Universidade de Coimbra'},
                                        {'title': 'Sacha Beniamine', 'organization': 'University of Surrey'},
                                        {'title': 'Ana R. Luís', 'organization': 'Universidade de Coimbra'},
                                        {'title': 'Olivier Bonami', 'organization': 'U. Paris-Sorbonne, IUF'},
                                        ],
                          version="2.0.2",
                          languages_iso639= ["por", ],
                          citation="Fernando Perdigão, Sacha Beniamine, Ana R. Luís and " \
                                   "Olivier Bonami (2021). European Portuguese Verbal Paradigms" \
                                   " in Phonemic Notation. DOI:10.5281/zenodo.5121543 [dataset]",
                          keywords=["portuguese", "paradigms"],
                          name="velepor")

package.infer()
package.custom["id"] = "http://dx.doi.org/10.5281/zenodo.5121543"
package.to_json("VeLePor.package.json")
