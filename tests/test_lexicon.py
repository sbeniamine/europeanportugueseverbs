#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from unittest import TestCase
import pandas as pd
import re

class TestLexicon(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.lexicon = pd.read_csv("portuguese-V.csv", index_col=0)
        cls.phonemes = pd.read_csv("portuguese_phonemes.csv", index_col=0)

    def test_columns(self):
        self.assertListEqual(self.lexicon.columns.tolist(),
                             ['cell', 'form', 'language_ID', 'POS'])

    def test_alphabet(self):
        """All sounds should be defined in the phonemes file"""
        legal_chars = "^("+"|".join(sorted(self.phonemes.index.tolist(), key=len, reverse=True))+")+$"
        is_legal = self.lexicon["form"].apply(lambda x: re.match(legal_chars, x) is not None)
        self.assertTrue(is_legal.all(), "Illegal characters found")

    def test_stress(self):
        """All forms should be stressed"""
        has_stress = self.lexicon["form"].str.contains("ˈ")
        self.assertTrue(has_stress.all(), "Unstressed forms found")